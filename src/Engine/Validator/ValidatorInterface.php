<?php
namespace Engine\Validator;

interface ValidatorInterface {

	public function validate($items, $rules = []);

	public function getErrors();

	public function addError($item, $error);

}