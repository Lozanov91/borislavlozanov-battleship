<?php
namespace Engine\Validator;

class GameValidator implements ValidatorInterface {

    private $_errors = [];

    public function validate($request, $rules = [])
    {
        foreach ($request as $var => $varValue) {

            if (key_exists($var, $rules)) {

                foreach ($rules[$var] as $rule => $ruleValue) {

                    if (is_int($rule)) {
                    	$rule = $ruleValue;
                    }

                    switch ($rule) {

                        case 'required':

	                        if (empty($varValue) || strlen($varValue) < 1 && $ruleValue) {
	                            $this->addError($var, '*** '.ucwords($var).' is required ***');
	                        }

                        break;

                        case 'minLen':

	                        if (strlen($varValue) < $ruleValue) {
	                        	$this->addError($var, '*** '.ucwords($var).' is Invalid ***');
	                        }

                        break;

                        case 'maxLen':

	                        if(strlen($varValue) > $ruleValue){
	                            $this->addError($var, '*** '.ucwords($var).' is Invalid ***');
	                        }

                        break;

                        case 'regex':
                            if(!preg_match($ruleValue, $varValue)){
                                $this->addError($var, '*** '.ucwords($var).' is Invalid ***');
                            }

                        break;

                    }

                }

            }

        }

        return $this;
    }

    public function addError($var, $error)
    {
        $this->_errors[$var] = $error;
    }

    public function getErrors()
    {
        return empty($this->_errors) ? null : $this->_errors;
    }

    public function isValid()
    {
        return empty($this->_errors) ? true : false;
    }
}
