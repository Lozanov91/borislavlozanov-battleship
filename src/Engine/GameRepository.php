<?php
namespace Engine;

use Models\Game;
use Models\Board;
use Models\Point;

abstract class GameRepository
{
    /**
	 * @param  Board 	$board
	 * @param  string 	$pointName
	 * @return Point 	$point|null
	 */
    protected function getPointByName(Board $board, string $pointName)
    {
    	$layout = $board->getLayout();

		foreach ($layout as $row => $rowColPoints) {
			foreach ($rowColPoints as $point) {
				return strtoupper($pointName) == strtoupper($point->getName()) ? $point : null;
			}
		}
	}

	/**
	 * @param  Board 	$board
	 * @param  int 	    $row
	 * @param  int 	    $col
	 * @return Point 	$point|null
	 */
    protected function getPointByPos(Board $board, int $row, int $col)
    {
    	$layout = $board->getLayout();

    	return isset( $layout[$row][$col] ) ? $layout[$row][$col] : null;
	}

	/**
	 * @param  array 	$ships
	 * @return int 	    $total
	 */
	protected function getTotalShipsCount(array $ships)
	{
		$total = 0;

		foreach ($ships as $ship) {
			$shipClass 	= 'Models\\'.$ship;
			$ship 		= $this->createShipByClass($shipClass);

		    $total += $ship->getLength();
		}

		return $total;
	}

	/**
	 * @param  string 	$shipClass
	 * @return Ship     $ship
	 */
	protected function createShipByClass($shipClass)
	{
		if (class_exists($shipClass)) {
			$ship = new $shipClass();
		} else {
			echo 'Invalid Ship Class: '.$shipClass;
			exit;
		}

		return $ship;
	}

	/**
	 * @return $_POST|null
	 */
	protected function getRequest()
	{
		if ($_POST && count($_POST)) {
			$request = [];

			foreach ($_POST as $var => $varValue) {
				$request[$var] = $varValue;
			}

			return $request;
		}

		return null;
	}

	/**
	 * @param  Game 	$game
	 * @param  string 	$coordinate
	 * @return Game 	$game
	 */
	protected function handleShot(Game $game, string $coord)
	{
		$game->increaseUserShots();

		$layout   = $game->getBoard()->getLayout();
		$hitPoint = null;
		$message  = '*** MISS ***';

		foreach ($layout as $row => $rowColPoints) {
			foreach ($rowColPoints as $point) {
				if (strtoupper($point->getName()) == strtoupper($coord)) {
					$hitPoint = $point;
				}
			}
		}

		if (!is_null($hitPoint)) {

			if ($hitPoint->hasShip()) {

				if (!$hitPoint->isHit()) {

					$message = '*** HIT ***';
					$hitPoint->getShip()->setShipHit();
					$hitPoint->setSymbol('X');

					$game->decreaseShipsLeft();

					if ($hitPoint->getShip()->getShotsLeftToSunk() == 0) {
						$message = '*** SUNK ***';
					}

				}

			} else {
				$hitPoint->setSymbol('-');
			}

			$hitPoint->setHit(true);
		}

		if ($game->getShipsLeft() > 0) {
			echo $message;
		}

		return $game;
	}

	/**
	 * @param  Game 	$game
	 */
	protected function handleCheatCommand(Game $game)
	{
		$layout = $game->getBoard()->getLayout();

		foreach ($layout as $row => $rowColPoints) {
			foreach ($rowColPoints as $point) {
				if (!$point->hasShip()) {

					$point->setSymbol(' ');

				} else {

					$point->setSymbol('X');

				}
			}
		}
	}

	/**
	 * @param  Game 	$game
	 * @return Game 	$game
	 */
	public function normalizeGameAfterCheat(Game $game)
	{
		$layout = $game->getBoard()->getLayout();

		foreach ($layout as $row => $rowColPoints) {
			foreach ($rowColPoints as $point) {
				$this->normalizePoint($point);
			}
		}

		return $game;
	}

	/**
	 * @param  Point 	$point
	 * @return Point 	$point
	 */
	public function normalizePoint(Point $point)
	{
		if ($point->hasShip() && $point->isHit()) {
			$point->setSymbol('X');
		} else if (!$point->isHit()) {
			$point->setSymbol('.');
		} else if ($point->isHit() && !$point->hasShip()) {
			$point->setSymbol('-');
		}

		return $point;
	}
}
