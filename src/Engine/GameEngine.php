<?php
namespace Engine;

use Engine\GameCreator;
use Engine\GameRepository;
use Engine\Validator\GameValidator;

use Storage\SessionStorage;

use Models\Game;

class GameEngine extends GameRepository
{
	private  $game;

	private  $gameCreator;
	private  $gameValidator;
	private  $sessionStorage;

	/**
	 * @param GameCreator    $gameCreator
	 * @param GameValidator  $gameValidator
	 * @param SessionStorage $sessionStorage
	 */
	public function __construct(
		GameCreator $gameCreator,
		GameValidator $gameValidator,
		SessionStorage $sessionStorage
	) {
		$this->gameCreator 	  = $gameCreator;
		$this->gameValidator  = $gameValidator;
		$this->sessionStorage = $sessionStorage;

		$this->runGame();
	}

	/**
	 * @return Game $game
	 */
	public function getRuntimeGame()
	{
		return $this->game;
	}

	/**
	 * @param Game $game
	 */
	public function setRuntimeGame(Game $game)
	{
		$this->game = $game;
	}

	public function createNewGame()
	{
		$newGame = $this->gameCreator->create();

		$this->setRuntimeGame($newGame);
		$this->sessionStorage->setGame($newGame);
	}

	public function runGame()
	{
		$runtimeGame = $this->sessionStorage->getGame();

		if (is_null($runtimeGame) || !$runtimeGame instanceof Game) {
			$runtimeGame = $this->gameCreator->create();
		}

		$this->setRuntimeGame($runtimeGame);
		$this->sessionStorage->setGame($runtimeGame);
	}

	public function play()
	{
		$request = $this->getRequest();
		$cheated = false;

		if (isset($request['coordinates'])) {
				
			$input = $request['coordinates'];

			if ($input == 'show') {

				$cheated = true;

				$this->handleCheatCommand($this->getRuntimeGame());
				
			} else if ($input == 'reset') {

				$this->createNewGame();

			} else {

				$validator = $this->gameValidator->validate($request, [
				    'coordinates' => ['required', 'minLen' => 2,'maxLen' => 5, 'regex' => '(^[a-zA-Z][1-9]|10)']
				]);

				if ($validator->isValid() !== true) {

					foreach ($validator->getErrors() as $error) {
						echo $error;
					}

				} else {

					$runtimeGame = $this->handleShot($this->getRuntimeGame(), $input);

					$this->setRuntimeGame($runtimeGame);
					$this->sessionStorage->setGame($runtimeGame);
				}

			}

		}

		if ($this->getRuntimeGame()->getShipsLeft() == 0) {
			echo '<h1>Well done!</h1>';
			echo 'You\'ve completed the game in '.$this->getRuntimeGame()->getUserShots().' shots.';
			echo '<p><a href="index.php">Start New Game?</a></p>';

			$this->createNewGame();
			exit;
		}

		echo $this;

		if ($cheated) {
			$normalizedGame = $this->normalizeGameAfterCheat($this->getRuntimeGame());
			$this->setRuntimeGame($normalizedGame);
			$this->sessionStorage->setGame($normalizedGame);
		}
	}

	/**
     * @return string
     */
    public function __toString()
    {
        $game   = $this->getRuntimeGame();
        $board  = $game->getBoard();

    	$render = '<pre>';
    	// draw columns header
		for ($col = 1; $col <= $board->getCols(); $col++) {
			// allign with points
			if ($col == 1) {
				$render .= ' ';
			}
			$render .= ' '.$col;
		}
		// set new line after header numbers
		$render .= PHP_EOL;
		foreach ($board->getLayout() as $row => $rowPoints) {
			// set alphabet letters
			$render .= $board->getLetter($row).' ';
			foreach ($rowPoints as $point) {
				$render .= $point->getSymbol().' ';
			}
			// set new line after row
			$render .= PHP_EOL;
		}
		$render .= '</pre>';
		$render .= '
			<form action="index.php" method="post">
				Enter coordinates (row, col), e.g. A5 
				<input type="text" name="coordinates" autofocus required="required">
				<input type="submit">
			</form>
		';

		return $render;
    }
}
