<?php
namespace Engine;

use Models\Game;
use Models\Board;
use Models\Ship;
use Models\Point;

class GameCreator extends GameRepository
{
	private $directions   = ['vertical', 'horizontal'];
	private $shipPosRetry = 1000;

	private $rows;
	private $cols;
	private $ships;

	public function __construct(
		$rows,
		$cols,
		$ships
	) {
		$this->rows  = $rows;
		$this->cols  = $cols;
		$this->ships = $ships;
	}

	public function create()
	{
		$board = new Board($this->rows, $this->cols);
		
		if ($this->createShipsOnBoard($board, $this->ships) !== true) {
			echo 'Cannot place ships after '.$this->shipPosRetry.' retries.';
			exit;
		}

		return new Game($board, $this->getTotalShipsCount($this->ships));
	}

	public function createShipsOnBoard(Board $board, array $ships)
	{
		foreach ($this->ships as $ship) {

			$shipClass 	= 'Models\\'.$ship;
			$ship 		= $this->createShipByClass($shipClass);

			$shipLength = $ship->getLength();
			$success 	= false;

			for ($i = 0; $i < $this->shipPosRetry; ++$i) {

				$randomRow = rand(1, $board->getRows());
				$randomCol = rand(1, $board->getCols());

				$direction = $this->getRandomDirection();

				if ($direction == 'vertical') {

					$canPositionUp    = $randomRow - $shipLength >= 0;
					$canPositionDown  = $randomRow + $shipLength <= $board->getRows();

					// can position UP
					if ($canPositionUp) {

						$tempShipPoints = [];

						for ($rowCord = $randomRow; $rowCord > $randomRow - $shipLength; $rowCord--) {
							$point = $this->getPointByPos($board, $rowCord, $randomCol);

							if ( !is_null($point) && is_null($point->getShip()) ) {
								array_push($tempShipPoints, $point);
							}
						}
						
						if ($this->tryToSetShipOnBoard($tempShipPoints, $ship)) {
							$success = true;
							break;
						}

					}

					// can position DOWN
					if ($canPositionDown) {
						
						$tempShipPoints = [];

						for ($rowCord = $randomRow; $rowCord < $randomRow + $shipLength; $rowCord++) {
							$point = $this->getPointByPos($board, $rowCord, $randomCol);

							if ( !is_null($point) && is_null($point->getShip()) ) {
								array_push($tempShipPoints, $point);
							}
						}

						if ($this->tryToSetShipOnBoard($tempShipPoints, $ship)) {
							$success = true;
							break;
						}

					}

				} else if ($direction == 'horizontal') {

					$canPositionLeft  = $randomCol - $shipLength >= 0;
					$canPositionRight = $randomCol + $shipLength <= $board->getCols();

					// can positon LEFT
					if ($canPositionLeft) {

						$tempShipPoints = [];

						for ($colCord = $randomCol; $colCord > $randomCol - $shipLength; $colCord--) {
							$point = $this->getPointByPos($board, $randomRow, $colCord);

							if ( !is_null($point) && is_null($point->getShip()) ) {
								array_push($tempShipPoints, $point);
							}
						}

						if ($this->tryToSetShipOnBoard($tempShipPoints, $ship)) {
							$success = true;
							break;
						}
					}

					// can positon RIGHT
					if ($canPositionRight) {

						$tempShipPoints = [];

						for ($colCord = $randomCol; $colCord < $randomCol + $shipLength; $colCord++) {
							$point = $this->getPointByPos($board, $randomRow, $colCord);

							if ( !is_null($point) && is_null($point->getShip()) ) {
								array_push($tempShipPoints, $point);
							}
						}

						if ($this->tryToSetShipOnBoard($tempShipPoints, $ship)) {
							$success = true;
							break;
						}
					}

				}

			}

			if (!$success) {
				return false;
			}
		}

		return true;
	}

	public function tryToSetShipOnBoard($tempShipPoints, Ship $ship)
	{
		if (count($tempShipPoints) == $ship->getLength()) {

			foreach($tempShipPoints as $point) {
				$point->setShip($ship);
			}

			return true;
		}

		return false;
	}

	public function getRandomDirection()
	{
		return rand(1, 1000) < 501 ? 'vertical' : 'horizontal';
	}
}
