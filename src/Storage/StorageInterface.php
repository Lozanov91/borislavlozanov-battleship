<?php
namespace Storage;

interface StorageInterface {

	/**
	 * @return Game
	 */
	public function getGame();
	
	/**
	 * @param Game $game
	 */
	public function setGame($game);

}
