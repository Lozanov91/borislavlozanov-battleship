<?php
namespace Storage;

class SessionStorage implements StorageInterface {

	/**
	 * @return Game
	 */
	public function getGame()
	{
		return isset($_SESSION['battleships']) ? unserialize($_SESSION['battleships']) : null;
	}

	/**
	 * @param Game $game
	 */
	public function setGame($game)
	{
		$_SESSION['battleships'] = serialize($game);
	}
}