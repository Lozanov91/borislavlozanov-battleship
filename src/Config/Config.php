<?php
namespace Config;

use Models\Battleship;
use Models\Destroyer;

class Config
{
	private $rows  = 10;
	private $cols  = 10;
	private $ships = [
		'Battleship',
		'Destroyer',
		'Destroyer'
	];

	/**
	 * @return int $rows
	 */
	public function getRows()
	{
		return $this->rows;
	}

	/**
	 * @return int $cols
	 */
	public function getCols()
	{
		return $this->cols;
	}

	/**
	 * @return array $ships
	 */
	public function getShips()
	{
		return $this->ships;
	}
}
