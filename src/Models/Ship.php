<?php
namespace Models;

interface Ship
{
	public function getlength();

	public function getShotsLeftToSunk();
}
