<?php
namespace Models;

class Game
{
	private $board;
	private $shipsLeft;
	private $userShots = 0;

	/**
	 * @param Board $board
	 * @param int   $totalShips
	 */
	public function __construct(Board $board, int $totalShips) {
		$this->board = $board;
		$this->setShipsLeft($totalShips);
	}

	/**
	 * @param int   $shipsLeft
	 */
	public function setShipsLeft(int $shipsLeft)
	{
		$this->shipsLeft = $shipsLeft;
	}

	/**
	 * @return int   $shipsLeft
	 */
	public function getShipsLeft() : int
	{
		return $this->shipsLeft;
	}

	/**
	 * @return int
	 */
	public function getUserShots() : int
	{
		return $this->userShots;
	}

	/**
	 * @return Board
	 */
	public function getBoard()
	{
		return $this->board;
	}

	/**
	 * @param Board $board
	 */
	public function setBoard(Board $board)
	{
		$this->board = $board;
	}

	/**
	 * @return int $userShots
	 */
	public function increaseUserShots()
	{
		$this->userShots += 1;
	}

	/**
	 * @return int $shipsLeft
	 */
	public function decreaseShipsLeft()
	{
		$this->shipsLeft -= 1;
	}

}
