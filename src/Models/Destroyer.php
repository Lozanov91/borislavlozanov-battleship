<?php
namespace Models;

use Models\Ship;

class Destroyer implements Ship
{
	private $length = 4;
	private $shotsLeftToSunk = 4;

	/**
	 * @return int $length
	 */
	public function getlength() : int
	{
		return $this->length;
	}

	/**
	 * @return int $shotsLeftToSunk
	 */
	public function getShotsLeftToSunk() : int
	{
		return $this->shotsLeftToSunk;
	}

	public function setShipHit()
	{
		$this->shotsLeftToSunk -= 1;
	}
}
