<?php
namespace Models;

use Models\Ship;

class Point
{
	private $name;
	public  $rowPos;
	public  $colPos;

	private $ship 	 = null;
	private $isHit   = false;
	private $symbol  = '.';

	/**
	 * @param string $name
	 * @param int 	 $rowPos
	 * @param int 	 $colPos
	 */
	public function __construct(string $name, int $rowPos, int $colPos) {
		$this->name 	= $name;
		$this->rowPos 	= $rowPos;
		$this->colPos 	= $colPos;
	}

	/**
	 * @return string $name
	 */
	public function getName() : string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name)
	{
		$this->name = $name;
	}

	/**
	 * @return string $symbol
	 */
	public function getSymbol() : string
	{
		return $this->symbol;
	}

	/**
	 * @param string $symbol
	 */
	public function setSymbol(string $symbol)
	{
		$this->symbol = $symbol;
	}

	/**
	 * @return Ship
	 */
	public function getShip()
	{
		return $this->ship;
	}

	/**
	 * @param Ship $ship
	 */
	public function setShip(Ship $ship)
	{
		$this->ship = $ship;
	}

	/**
	 * @param bool $isHit
	 */
	public function setHit(bool $isHit)
	{
		$this->isHit = $isHit;
	}

	/**
	 * @return true|false
	 */
	public function isHit() : bool
	{
		return $this->isHit === true;
	}

	/**
	 * @return true|false
	 */
	public function hasShip() : bool
	{
		return $this->ship !== null;
	}

}
