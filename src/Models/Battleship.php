<?php
namespace Models;

use Models\Ship;

class Battleship implements Ship
{
	private $length = 5;
	private $shotsLeftToSunk = 5;

	/**
	 * @return int $length
	 */
	public function getlength() : int
	{
		return $this->length;
	}

	/**
	 * @return int $shotsLeftToSunk
	 */
	public function getShotsLeftToSunk() : int
	{
		return $this->shotsLeftToSunk;
	}

	public function setShipHit()
	{
		$this->shotsLeftToSunk -= 1;
	}
}
