<?php
namespace Models;

class Board
{
	private $rows;
	private $cols;
	private $letters;
	private $layout = [];

	/**
	 * @param int $rows
	 * @param int $cols
	 */
	public function __construct(int $rows, int $cols) {
		$this->rows = $rows;
		$this->cols = $cols;

		$this->setLetters($cols);
		$this->create();
	}

	public function create()
	{
		for ($row = 1; $row <= $this->rows; $row++) {

			$this->layout[$row] = [];

			for ($col = 1; $col <= $this->cols; $col++) {
				$this->layout[$row][$col] = new Point($this->getLetter($row).$col, $row, $col);
			}

		}
	}

	/**
	 * @param int $cols
	 */
	public function setLetters(int $cols)
	{
		$this->letters = range('A', chr(64 + $cols));
	}

	/**
	 * @return array $letters
	 */
	public function getLetters() : array
	{
		return $this->letters;
	}

	/**
	 * @return string
	 */
	public function getLetter(int $pointer) : string
	{
		return $this->getLetters()[$pointer - 1];
	}

	/**
	 * @return int $rows
	 */
	public function getRows() : int
	{
		return $this->rows;
	}

	/**
	 * @return int $cols
	 */
	public function getCols() : int
	{
		return $this->cols;
	}

	/**
	 * @return array $layout
	 */
	public function getlayout() : array
	{
		return $this->layout;
	}

}
