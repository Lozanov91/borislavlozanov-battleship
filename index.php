<?php
require __DIR__ . '/vendor/autoload.php';
error_reporting(E_ALL);
session_start();

/*
	Battleships
*/
use Config\Config;
use Storage\SessionStorage;

use Engine\GameEngine;
use Engine\GameCreator;
use Engine\Validator\GameValidator;

$config 		= new Config();
$gameEngine     = new GameEngine(
					new GameCreator($config->getRows(), $config->getCols(), $config->getShips()),
					new GameValidator(),
					new SessionStorage()
				);

$gameEngine->play();
